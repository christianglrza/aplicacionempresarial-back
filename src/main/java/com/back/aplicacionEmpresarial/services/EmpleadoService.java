package com.back.aplicacionEmpresarial.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;

import com.back.aplicacionEmpresarial.models.Empleado;
import com.back.aplicacionEmpresarial.repository.EmpleadoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class EmpleadoService {
    @Autowired
    private EmpleadoRepository empleadoRepository;

    public ArrayList<Empleado> obtenerEmpleados(){
        return (ArrayList<Empleado>) empleadoRepository.findAll();
    }

    public Empleado guardar(String brm,String nombre,String puesto,MultipartFile foto){
        Empleado e = new Empleado();
        e.setBrm(brm);
        e.setNombre(nombre);
        e.setPuesto(puesto);
        try {
            e.setFoto(Base64.getEncoder().encodeToString(foto.getBytes()));
        } catch (IOException error) {
            error.printStackTrace();
        }
        return empleadoRepository.save(e);
    }

    public Empleado actualizar(Long id, String brm,String nombre,String puesto,MultipartFile foto){
        return empleadoRepository.findById(id)
                .map(empleado -> {
                   empleado.setBrm(brm);
                    try {
                        empleado.setFoto(Base64.getEncoder().encodeToString(foto.getBytes()));
                    } catch (IOException error) {
                        error.printStackTrace();
                    }
                   empleado.setNombre(nombre);
                   empleado.setPuesto(puesto);
                   return empleadoRepository.save(empleado);
                })
                .orElseGet(() -> {
                    Empleado e = new Empleado();
                    return empleadoRepository.save(e);
                });
    }

    public boolean eliminar(Long id){
        try {
            empleadoRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
