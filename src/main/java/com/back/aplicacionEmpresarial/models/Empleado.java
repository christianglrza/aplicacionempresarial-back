package com.back.aplicacionEmpresarial.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="empleados")
public class Empleado {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY) 
  @Column(unique = true,nullable = false)
  private Long id;

  private String brm;
  private String nombre;
  @Lob
  private String foto;
  private String puesto;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getBrm() {
        return brm;
    }
    public void setBrm(String brm) {
        this.brm = brm;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getFoto() {
        return foto;
    }
    public void setFoto(String foto) {
        this.foto = foto;
    }
    public String getPuesto() {
        return puesto;
    }
    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }
}
