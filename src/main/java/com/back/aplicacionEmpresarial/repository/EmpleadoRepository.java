package com.back.aplicacionEmpresarial.repository;

import com.back.aplicacionEmpresarial.models.Empleado;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpleadoRepository extends JpaRepository<Empleado, Long>{
    
}
