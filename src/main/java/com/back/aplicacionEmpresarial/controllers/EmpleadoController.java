package com.back.aplicacionEmpresarial.controllers;

import java.net.URI;
import java.util.ArrayList;
import java.util.Optional;

import com.back.aplicacionEmpresarial.models.Empleado;
import com.back.aplicacionEmpresarial.services.EmpleadoService;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/empleados")
public class EmpleadoController {
    @Autowired
    private EmpleadoService empleadoService;

    @GetMapping()
    public ResponseEntity<ArrayList<Empleado>> obtenerEmpleados(){
        return ResponseEntity.ok(empleadoService.obtenerEmpleados());
    }

    @PostMapping()
    public ResponseEntity<Empleado> guardar(@RequestParam("brm") String brm,@RequestParam("nombre") String nombre,@RequestParam("puesto") String puesto,@RequestParam("foto") MultipartFile foto){
        try {
            Empleado empleado = empleadoService.guardar(brm, nombre, puesto, foto);
            return ResponseEntity.created(new URI("/empleados/" + empleado.getId())).body(empleado);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<Empleado> actualizar(@PathVariable("id") Long id, @RequestParam("brm") String brm,@RequestParam("nombre") String nombre,@RequestParam("puesto") String puesto,@RequestParam("foto") MultipartFile foto){
        try {
            return ResponseEntity.ok(empleadoService.actualizar(id, brm, nombre, puesto, foto));
        } catch (Exception e) {
            return (ResponseEntity<Empleado>) ResponseEntity.status(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Boolean> eliminar(@PathVariable("id") Long id){
        try {
            return ResponseEntity.ok(empleadoService.eliminar(id));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
